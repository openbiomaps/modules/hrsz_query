ALTER TABLE public."dhte_erdoreszletek" ADD COLUMN obm_first_letter_filter character(1);
UPDATE public.dhte_erdoreszletek SET obm_first_letter_filter=lower(substring(telepules from 1 for 1));
EXPLAIN ANALYZE SELECT DISTINCT "telepules" "telepules" FROM public."hrsz_2018" WHERE obm_first_letter_filter='g' AND telepules ILIKE 'gárd%' ORDER BY telepules LIMIT 20;
