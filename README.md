# Helyrajzi szám lekérdező modul

A modul célja nagy méretű postgis táblában keresni, és az abban található geometriák alapján lekérdezéseket végehajtani egy adott projekttáblában.
Mivel egyedi modulról van szó, használat előtt a modulban és a hozzá kapcsolódó sql parancsban a tábla és mezőneveket, címet aktualizálni kell.

## Előfeltételek

### Definiálás projekttáblaként

Azt a táblát, ami alapján majd a leklérdezéseket hajtjuk végre, hozzá kell adni az adott projekt projekttábláihoz.
Ehhez a biomaps adatbázis public.header_names táblájába kell beszúrni egy sort, amelyben legalább az alábbi mezők ki vannak töltve:

* f_table_schema: A projekttábla sémája a gisdata adatbázisban.
* f_table_name: A projekttábla, amelyből az adatokat kérdezzük le.
* f_id_column: A tábla egyedi azonosítóját tartalmazó mező neve.
* f_srid: A tábla vetületének epsg azonosítója.
* f_geom_column: A táblában a geometriát tartalmazó oszlop neve.
* f_main_table: A tábla neve.

### A gyorsított keresés előkészítése

Mivel nagy mennyiségű adatot kell használni, egy kiválasztott mező (pl. helyrajzi számok esetén a településnév) segítségével egy előszűrést hajt végre a modul, így csökkenti a keresési időt.
Ehhez a fenti mező tartalmának első betűjét egy új obm_first_letter_filter nevű oszlopba kell beírni a first_letter_filter.sql script segítségével.

## A modul beállítása

A modult a projekt könyvtárban az includes/modules/private mappában kell elhelyezni. Amennyiben szükséges, létre kell hozni a könyvtárat. A könyvtár jogosultságait célszerű úgy beállítani, hogy a www-data felhasználónak ne legyen írási jogosultsága. Ezzel elkerülhető, hogy az egyénileg létrehozott moduljaink felülíródjanak egy frissítés során.

A modult az OBM webes felületen az Adminisztráció/Modulok menüpontban tudjuk hozzáadni, ahol az alábbi értékeket kell beállítani.

* Modul név: box_custom
* Paraméterek: A modul fájlneve kiterjesztés nélkül.
* Engedélyezett: igen
* Hozzáférések: Igény szerint.

A megfelelő értékek megadása után a Módosít felirató gombra kattintva aktiválódik a modul a térképi nézeten.

## A modul használata

A használat egyszerű. A szűréshez használt mezőben található értéket begépelve a találatok megjelennek a mező alatt. Kattintással kiválasztjuk a megfelelőt. Az alsó mezőben megjelennek a szűrt értékek a lekérdezéshez használt mezőben, ahol egérkattintással választhatunk ki egy, valamint a SHIFT és/vagy a CTRL billentyűk segítségével egyszerre több elemet.
A térkép végigpörgeti, felvillantja a lekérdezett elemeket, a Kérdés gombra kattintva pedig megkapjuk a végső adatokat a projekt beállításainak megfelelő formá(k)ban.


